<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require "../start.php";
use Src\Material;
use Src\Type;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

if ($uri[1] == 'materials') {
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $controller = new Material($dbConnection, $requestMethod, false);
    $controller->proccessRequest();
} else {
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $controller = new Type($dbConnection, $requestMethod, false);
    $controller->proccessRequest();
}
