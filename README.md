# api-native

instalasi 
1. config database pada file .env 
   sesuaikan credentials dengan database lokal
2. import database pada file api_native.sql
3. jalankan php server
    php -S localhost:8000 -t api

// endpoint materials 
localhost:8000/materials <br>
{
    "materials": [
        {
            "id": "MAT-001-RES",
            "name": "Residu"
        },
        {
            "id": "MAT-002-PLS",
            "name": "Plastik"
        },
        {
            "id": "MAT-003-UBC",
            "name": "UBC"
        },
        {
            "id": "MAT-004-LOG",
            "name": "Logam"
        }
    ]
}
<br>

// endpoint type 
localhost:8000/type <br>
{
    "types": [
        {
            "id": "TYP-001-RES",
            "material_id": "MAT-001-RES",
            "name": "Residu"
        },
        {
            "id": "TYP-002-EC",
            "material_id": "MAT-002-PLS",
            "name": "Emberan Campuran"
        },
        {
            "id": "TYP-002-GP",
            "material_id": "MAT-002-PLS",
            "name": "Gelas (PP)"
        },
        {
            "id": "TYP-003-UBC",
            "material_id": "MAT-003-UBC",
            "name": "UBC"
        },
        {
            "id": "TYP-004-COC",
            "material_id": "MAT-004-LOG",
            "name": "Botol Coca Cola"
        }
    ]
}