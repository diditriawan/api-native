<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
require 'vendor/autoload.php';
use Src\Database;


$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

// echo $_ENV['DB_HOST'];
$dbConnection = (new Database())->connet();