<?php
namespace Src;

interface initRest {
    const messageNotFound = 'HTTP/1.1 404 Not Found';
    const messageOk = 'HTTP/1.1 200 OK';
}

abstract class connectDb {
    public $name;
    public $db;
    public $requestMethod;
    public $postId;

    public function __construct($db, $requestMethod, $postId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->postId = $postId;
    }
    abstract public function proccessRequest();
}

class Material extends connectDb implements initRest {
    

    public function proccessRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                $response = $this->getAllMaterials();
                break;
            default:
                $response = $this->notFoundResponse();
            break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    /**
     * static function init select table
     *
     * @return void
     */
    public function getAllMaterials()
    {
        $query = "
        SELECT
           *
        FROM
            materials;
        ";

        try {
            $statement = $this->db->query($query);
            $result['materials'] = $statement->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        $response['status_code_header'] = initRest::messageOk;
        $response['body'] = json_encode($result);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = initRest::messageNotFound;
        $response['body'] = null;
        return $response;
    }
}